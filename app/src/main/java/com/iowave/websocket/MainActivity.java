package com.iowave.websocket;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.sql.Time;
import java.util.ArrayList;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class MainActivity extends AppCompatActivity {

    private Button start, stop;
    private TextView output;
    private EditText input;
    private OkHttpClient client;
    private Request request;
    private boolean socketIpCreated;

    private ArrayList<String> socketIps = new ArrayList<>();
    private ArrayList<WebSocket> wsList = new ArrayList<>();
    private final class EchoWebSocketListener extends WebSocketListener {

        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            super.onClosed(webSocket, code, reason);
        }

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            Log.i("MAIN", "onOpen");

            //  webSocket.close(NORMAL_CLOSURE_STATUS, "Goodbye !");
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Log.i("MAIN", "onMessagetext" );
            output("Receiving : " + text);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            Log.i("MAIN", "onMessage");
            output("Receiving bytes : " + bytes.hex());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            Log.i("MAIN", "onClosing");
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            output("Closing : " + code + " / " + reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            Log.i("MAIN", "onFailure");
            output("Error : " + t.getMessage());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button) findViewById(R.id.start);
        output = (TextView) findViewById(R.id.output);
        input = (EditText) findViewById(R.id.input);
        stop = (Button) findViewById(R.id.stop);

        socketIps.add("http://192.168.1.10:80");
        socketIps.add("http://192.168.1.2:80");

        client = new OkHttpClient();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = input.getEditableText().toString();
                start(data);
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wsList.size() >0) {
                    for(WebSocket ws :wsList){
                        ws.close(EchoWebSocketListener.NORMAL_CLOSURE_STATUS, "Goodbye !");
                    }
                    request = null;
                    client = null;
                    socketIpCreated = false;
                    wsList.clear();
                }

            }
        });

    }

    private void start(String data) {

        if (client == null) {
            Dispatcher dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(100);
            dispatcher.setMaxRequestsPerHost(100);
            client = new OkHttpClient.Builder()
            .dispatcher(dispatcher)
                    .build();

        }

        if(!socketIpCreated) {
            for (int i = 0; i < socketIps.size(); i++) {
                request = new Request.Builder().url(socketIps.get(i)).build();
                wsList.add(client.newWebSocket(request, new EchoWebSocketListener()));
                socketIpCreated = true;

            }
        }

        for(int i = 0 ;i<wsList.size() ; i++){
            wsList.get(i).send(data);
            output("SEND : " + data);
        }
        input.setText("");

        //  client.dispatcher().executorService().shutdown();
    }

    private void output(final String txt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                output.setText(output.getText().toString() + "\n\n" + txt);
            }
        });
    }
}
